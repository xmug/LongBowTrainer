﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class HandInteractor : XRDirectInteractor
{

    [Header("Sounds")]
    public AudioClip bowGrabClip;
    public AudioClip arrowGrabClip;

    public void ForceInteract(SelectEnterEventArgs args)
    {
        OnSelectEntered(args);
    }

    public void ForceDeinteract(SelectExitEventArgs args)
    {
        OnSelectExited(args);
    }

    public void HandDetection(SelectEnterEventArgs args)
    {
        if(args.interactable is Arrow arrow)
        {
            HandSounds(arrowGrabClip, 3.5f, 3,.8f, 7);
        }

        if (args.interactable is Bow bow)
        {
            HandSounds(bowGrabClip, 2.5f, 2.5f,.8f, -3);
        }
    }


    void HandSounds(AudioClip clip, float minPitch, float maxPitch,float volume, int id)
    {
        SFXPlayer.Instance.PlaySFX(clip, transform.position, new SFXPlayer.PlayParameters()
        {
            Pitch = Random.Range(minPitch, maxPitch),
            Volume = volume,
            SourceID = id
        });
    }
}
