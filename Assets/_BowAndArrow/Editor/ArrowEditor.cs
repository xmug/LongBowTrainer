﻿using UnityEditor;
using UnityEditor.XR.Interaction.Toolkit;

[CustomEditor(typeof(Arrow))]
public class ArrowEditor : XRGrabInteractableEditor
{
    private SerializedProperty speed = null;
    public SerializedProperty tip = null;
    public SerializedProperty layerMask = null;
    public SerializedProperty trailParticle = null;
    public SerializedProperty hitParticle = null;
    public SerializedProperty trailRenderer = null;
    public SerializedProperty launchClip = null;
    public SerializedProperty hitClip = null;

    protected override void OnEnable()
    {
        base.OnEnable();

        speed = serializedObject.FindProperty("speed");
        tip = serializedObject.FindProperty("tip");
        layerMask = serializedObject.FindProperty("layerMask");
        trailParticle = serializedObject.FindProperty("trailParticle");
        hitParticle = serializedObject.FindProperty("hitParticle");
        trailRenderer = serializedObject.FindProperty("trailRenderer");
        launchClip = serializedObject.FindProperty("launchClip");
        hitClip = serializedObject.FindProperty("hitClip");
    }

    protected override void DrawCoreConfiguration()
    {
        base.DrawCoreConfiguration();

        EditorGUILayout.Space();
        EditorGUILayout.LabelField("Arrow", EditorStyles.boldLabel);

        EditorGUILayout.PropertyField(speed);
        EditorGUILayout.PropertyField(tip);
        EditorGUILayout.PropertyField(layerMask);
        EditorGUILayout.PropertyField(trailParticle);
        EditorGUILayout.PropertyField(hitParticle);
        EditorGUILayout.PropertyField(trailRenderer);
        EditorGUILayout.PropertyField(launchClip);
        EditorGUILayout.PropertyField(hitClip);
    }
}
