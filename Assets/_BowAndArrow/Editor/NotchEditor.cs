﻿using UnityEditor;
using UnityEditor.XR.Interaction.Toolkit;

[CustomEditor(typeof(Notch))]
public class NotchEditor : XRSocketInteractorEditor
{
    private SerializedProperty attatchClip = null;

    protected override void OnEnable()
    {
        base.OnEnable();
        
        attatchClip = serializedObject.FindProperty("attatchClip");
    }

    protected override void DrawCoreConfiguration()
    {
        base.DrawCoreConfiguration();

        EditorGUILayout.Space();
        EditorGUILayout.LabelField("References", EditorStyles.boldLabel);

        // EditorGUILayout.PropertyField(releaseThreshold);
        EditorGUILayout.PropertyField(attatchClip);
    }
}
