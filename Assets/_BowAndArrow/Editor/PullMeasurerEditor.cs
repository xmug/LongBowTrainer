﻿using UnityEditor;
using UnityEditor.XR.Interaction.Toolkit;

[CustomEditor(typeof(PullMeasurer))]
public class PullMeasurerEditor : XRBaseInteractableEditor
{
    private SerializedProperty start = null;
    private SerializedProperty end = null;
    private SerializedProperty lineParticle = null;
    private SerializedProperty lefthand = null;
    private SerializedProperty righthand = null;
    private SerializedProperty bow = null;

    protected override void OnEnable()
    {
        base.OnEnable();
        start = serializedObject.FindProperty("start");
        end = serializedObject.FindProperty("end");
        lineParticle = serializedObject.FindProperty("lineParticle");
        lefthand = serializedObject.FindProperty("lefthand");
        righthand = serializedObject.FindProperty("righthand");
        bow = serializedObject.FindProperty("bow");
    }

    protected override void DrawCoreConfiguration()
    {
        base.DrawCoreConfiguration();

        EditorGUILayout.Space();
        EditorGUILayout.LabelField("References", EditorStyles.boldLabel);

        EditorGUILayout.PropertyField(start);
        EditorGUILayout.PropertyField(end);
        EditorGUILayout.PropertyField(lineParticle);
        EditorGUILayout.PropertyField(lefthand);
        EditorGUILayout.PropertyField(righthand);
        EditorGUILayout.PropertyField(bow);
    }
}
